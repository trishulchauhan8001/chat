from django.db import models
from django.core.files.base import ContentFile
from django.contrib.auth.models import User


class Room(models.Model):
    roomID = models.AutoField(primary_key=True, db_index=True)
    creator = models.ForeignKey(User, related_name='creator', on_delete=models.CASCADE)
    invited = models.ManyToManyField(User, related_name="invited_user")
    roomName = models.TextField(max_length=50)
    date = models.DateTimeField(auto_now_add=True)


class Chat(models.Model):
    chatID = models.AutoField(primary_key=True, db_index=True)
    room = models.ForeignKey(Room, related_name='room', on_delete=models.CASCADE)
    user = models.ForeignKey(User, related_name='chatuser', on_delete=models.CASCADE)
    content = models.TextField(max_length=500, null=True)
    date = models.DateTimeField(auto_now_add=True)
    documentFile = models.FileField(upload_to='aap/img', null=True)

    def save_file(self, content: str):
        temp_file = ContentFile(content)
        self.cotent.save(f'{self.pk}.png', temp_file)
