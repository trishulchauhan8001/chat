from django.contrib.auth.decorators import login_required
from django.shortcuts import get_object_or_404
from django.contrib.auth import get_user_model
from django.shortcuts import render
from .models import Chat, Room
from django.utils.safestring import mark_safe
import json
from django.http import HttpResponse, HttpResponseBadRequest

User = get_user_model()

@login_required
def room(request):
    room = get_object_or_404(Room, roomID=1)

    return render(request, 'chat/room.html', {
        'room_name_json': mark_safe(json.dumps(str(room.roomName))),
        'username': mark_safe(json.dumps(request.user.username)),
        'roomID' : mark_safe(json.dumps(str(room.roomID)))
    })



User = get_user_model()

# return chat.objects.order_by('-date').all()[:10]

def get_last_10_messages(roomID):
    return Chat.objects.filter(room=roomID).order_by('date')[10:]



def get_current_chat(chatId):
    return get_object_or_404(Chat, id=chatId)